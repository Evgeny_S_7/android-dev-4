package com.example.guitar30days.model

import com.example.guitar30days.R

object Repository {
    val Tips = listOf(
        Tip(
            nameRes = R.string.tip_title_1,
            instructionRes = R.string.tip_desc_1,
            imageRes = R.drawable.tip_img_1,
        ),
        Tip(
            nameRes = R.string.tip_title_2,
            instructionRes = R.string.tip_desc_2,
            imageRes = R.drawable.tip_img_2,
        ),
        Tip(
            nameRes = R.string.tip_title_3,
            instructionRes = R.string.tip_desc_3,
            imageRes = R.drawable.tip_img_3,
        ),
        Tip(
            nameRes = R.string.tip_title_4,
            instructionRes = R.string.tip_desc_4,
            imageRes = R.drawable.tip_img_4,
        ),
        Tip(
            nameRes = R.string.tip_title_5,
            instructionRes = R.string.tip_desc_5,
            imageRes = R.drawable.tip_img_5,
        ),
        Tip(
            nameRes = R.string.tip_title_6,
            instructionRes = R.string.tip_desc_6,
            imageRes = R.drawable.tip_img_6,
        ),
        Tip(
            nameRes = R.string.tip_title_7,
            instructionRes = R.string.tip_desc_7,
            imageRes = R.drawable.tip_img_7
        ),
        Tip(
            nameRes = R.string.tip_title_8,
            instructionRes = R.string.tip_desc_8,
            imageRes = R.drawable.tip_img_8,
        ),
        Tip(
            nameRes = R.string.tip_title_9,
            instructionRes = R.string.tip_desc_9,
            imageRes = R.drawable.tip_img_9,
        ),
        Tip(
            nameRes = R.string.tip_title_10,
            instructionRes = R.string.tip_desc_10,
            imageRes = R.drawable.tip_img_10,
        ),
        Tip(
            nameRes = R.string.tip_title_11,
            instructionRes = R.string.tip_desc_11,
            imageRes = R.drawable.tip_img_11,
        ),
        Tip(
            nameRes = R.string.tip_title_12,
            instructionRes = R.string.tip_desc_12,
            imageRes = R.drawable.tip_img_12,
        ),
        Tip(
            nameRes = R.string.tip_title_13,
            instructionRes = R.string.tip_desc_13,
            imageRes = R.drawable.tip_img_13,
        ),
        Tip(
            nameRes = R.string.tip_title_14,
            instructionRes = R.string.tip_desc_14,
            imageRes = R.drawable.tip_img_14,
        ),
        Tip(
            nameRes = R.string.tip_title_15,
            instructionRes = R.string.tip_desc_15,
            imageRes = R.drawable.tip_img_15,
        ),
        Tip(
            nameRes = R.string.tip_title_16,
            instructionRes = R.string.tip_desc_16,
            imageRes = R.drawable.tip_img_16,
        ),
        Tip(
            nameRes = R.string.tip_title_17,
            instructionRes = R.string.tip_desc_17,
            imageRes = R.drawable.tip_img_17,
        ),
        Tip(
            nameRes = R.string.tip_title_18,
            instructionRes = R.string.tip_desc_18,
            imageRes = R.drawable.tip_img_18,
        ),
        Tip(
            nameRes = R.string.tip_title_19,
            instructionRes = R.string.tip_desc_19,
            imageRes = R.drawable.tip_img_19,
        ),
        Tip(
            nameRes = R.string.tip_title_20,
            instructionRes = R.string.tip_desc_20,
            imageRes = R.drawable.tip_img_20,
        ),
        Tip(
            nameRes = R.string.tip_title_21,
            instructionRes = R.string.tip_desc_21,
            imageRes = R.drawable.tip_img_21,
        ),
        Tip(
            nameRes = R.string.tip_title_22,
            instructionRes = R.string.tip_desc_22,
            imageRes = R.drawable.tip_img_22,
        ),
        Tip(
            nameRes = R.string.tip_title_23,
            instructionRes = R.string.tip_desc_23,
            imageRes = R.drawable.tip_img_23,
        ),
        Tip(
            nameRes = R.string.tip_title_24,
            instructionRes = R.string.tip_desc_24,
            imageRes = R.drawable.tip_img_24,
        ),
        Tip(
            nameRes = R.string.tip_title_25,
            instructionRes = R.string.tip_desc_25,
            imageRes = R.drawable.tip_img_25,
        ),
        Tip(
            nameRes = R.string.tip_title_26,
            instructionRes = R.string.tip_desc_26,
            imageRes = R.drawable.tip_img_26,
        ),
        Tip(
            nameRes = R.string.tip_title_27,
            instructionRes = R.string.tip_desc_27,
            imageRes = R.drawable.tip_img_27,
        ),
        Tip(
            nameRes = R.string.tip_title_28,
            instructionRes = R.string.tip_desc_28,
            imageRes = R.drawable.tip_img_28,
        ),
        Tip(
            nameRes = R.string.tip_title_29,
            instructionRes = R.string.tip_desc_29,
            imageRes = R.drawable.tip_img_29,
        ),
        Tip(
            nameRes = R.string.tip_title_30,
            instructionRes = R.string.tip_desc_30,
            imageRes = R.drawable.tip_img_30,
        ),

    )

}